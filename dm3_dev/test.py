from subdialoguemanager import Operator
from subdialoguemanager import IfThenElse
from subdialoguemanager import UserSays
from subdialoguemanager import Condition
from subdialoguemanager import Steps
from subdialoguemanager import State
from subdialoguemanager import Subdialogue

subdialogue = Subdialogue(name="SimonSaysRoundM")
"""
  {
    "topic": "init",
    "condition": ["==", "state", "init"],
    "steps": [
      {"set": "state", "val": "question"}
    ]
  },
"""

state = State(topic="init")

condition = Condition()
condition.addConditional(operator=Operator.EQUAL,name="state",value="init")
state.setCondition(condition=condition.get())

steps = Steps()
steps.add(step={"set": "state", "val": "question"})

state.setSteps(steps=steps.get())
subdialogue.addState(state=state)

"""
  {
    "topic": "timeout-warning",
    "parallel": true,
    "condition": ["&&", "system.is-waiting-on-user", [">=", "system.timer", 10]],
    "steps": [
      {"set": "system.timer", "val": 0},
      {"reward": 10},
      {"show": {"system": "timeout"}},
      {"userSays": [
        ["system.timeout.ack.pos"], [
          {"set": "state", "val": "timeout"}
        ]
      ]}
    ]
  },"""

state = State(topic="timeout-warning")
state.parallelize()

condition = Condition()
condition.add(aCondition="system.is-waiting-on-user")
condition.addConditional(operator=Operator.GTE, name="numRetries", value=3)
state.setCondition(condition=condition.get())

steps = Steps()
steps.add(step={"set": "system.timer", "val": "0"})
steps.add(step={"reward": "10"})
steps.add(step={"show": {"system": "timeout"}})

userSays = UserSays()
userSays.add(ifUserSays=["system.timeout.ack.pos"], thenAgentResponds={"set": "state", "val": "timeout"})
steps.add(step=userSays.get())

state.setSteps(steps=steps.get())
subdialogue.addState(state=state)

"""
    {
    "topic": "timeout",
    "condition": ["==", "state", "timeout"],
    "steps": [
      {"set": "system.timer", "val": 0},
      {
        "if": ["==", "numRetries", 0],
        "then": {"say": "command.listen"},
        "else": {"say": "question.stillplaying"}
      },
      {"set": "state", "val": "@resume"},
      {"inc": "numRetries"}
    ]
  },
"""
state = State(topic="timeout")

condition = Condition()
condition.addConditional(operator=Operator.EQUAL, name="state", value=0)
state.setCondition(condition=condition.get())

steps = Steps()
steps.add(step={"set": "system.timer", "val":0})

ifthenelse = IfThenElse()
condition = Condition()
condition.addConditional(operator=Operator.EQUAL, name="numRetries", value=0)
ifthenelse.setIf(if_part=condition.get())
ifthenelse.addThen({"say": "command.listen"})
ifthenelse.addElse({"say": "question.stillplaying"})
steps.add(step=ifthenelse.get())

steps.add(step={"set": "state", "val": "@resume"})
steps.add(step={"inc": "numRetries"})

state.setSteps(steps=steps.get())
subdialogue.addState(state=state)

"""
    {
    "topic": "set.say-mymushymango",
    "condition": ["&&", ["==", "letter", "m"], ["==", "state", "say-x"], [">", "numQuestionsAsked", 0], "!seen-mymushymango"],
    "steps": [
      {"set": "seen-mymushymango", "val": true},
      {"set": "state", "val": "say-mymushymango"},
      {"set": "resume", "val": "@state"}
    ]
  },"""

state = State(topic="set.say-mymushymango")

condition = Condition()
condition.addConditional(operator=Operator.EQUAL,name="letter",value="m")
condition.addConditional(operator=Operator.EQUAL,name="state",value="say-x")
condition.addConditional(operator=Operator.GT,name="numQuestionsAsked",value=0)
condition.add(aCondition="!seen-mymushymango")
state.setCondition(condition.get())

steps = Steps()
steps.add(step={"set": "seen-mymushymango", "val": True})
steps.add(step={"set": "state", "val": "say-mymushymango"})
steps.add(step={"set": "resume", "val": "@state"})

state.setSteps(steps=steps.get())
subdialogue.addState(state=state)

"""
    {
    "topic": "catch.say-m",
    "condition": ["&&", ["==", "state", "catch-say-letter"], ["==", "letter", "m"]],
    "steps": [
      {"set": "system.timer", "val": 0},
      {"userSays": [
        ["statement.title.letter.m", "ack.neg"], [
          {
            "if": ["==", "simonSaysIndicator", 1],
            "then": [
              {"say": "sfx.ss.correctshimmer"},
              {"say": "ack.pos.kids"},
              {"set": "state", "val": "correct"}
            ],
            "else":
              {"say": "ack.neg.nosimonsays"}
          }
        ],
        ["statement.title.letter", "system.answer.wrong"], [
          {"say": "ack.neg.wrongresponse"},
          {"say": "command.listen"},
          {"set": "state", "val": "wrong"}
        ]
      ]}
    ]
  },
  """

state = State(topic="catch.say-m")

condition = Condition()
condition.addConditional(operator=Operator.EQUAL,name="letter",value="m")
condition.addConditional(operator=Operator.EQUAL,name="state",value="catch-say-letter")
state.setCondition(condition.get())

steps = Steps()
steps.add(step={"set": "system.timer", "val": "0"})

ifthenelse = IfThenElse()
condition = Condition()
condition.addConditional(operator=Operator.EQUAL, name="simonSaysIndicator", value=1)
ifthenelse.setIf(if_part=condition.get())
ifthenelse.addThen(then_part={"say": "command.listen"})
ifthenelse.addThen(then_part={"say": "ack.pos.kids"},)
ifthenelse.addThen(then_part={"set": "state", "val": "correct"})
ifthenelse.addElse(else_part={"say": "ack.neg.nosimonsays"})
userSays.add(ifUserSays=["statement.title.letter.m", "ack.neg"], thenAgentResponds=ifthenelse.get())
userSays.add(ifUserSays=["statement.title.letter", "system.answer.wrong"], thenAgentResponds={"say": "ack.neg.wrongresponse"})
userSays.add(ifUserSays=["statement.title.letter", "system.answer.wrong"], thenAgentResponds={"say": "command.listen"})
userSays.add(ifUserSays=["statement.title.letter", "system.answer.wrong"], thenAgentResponds={"set": "state", "val": "wrong"})
steps.add(step=userSays.get())

state.setSteps(steps=steps.get())
subdialogue.addState(state=state)

"""
    {
    "topic": "catch.say-makemadface",
    "condition": ["&&", ["==", "state", "catch-say-makemadface"]],
    "steps": [
      {"set": "system.timer", "val": 0},
      {"userSays": [
        ["vision.angry", "ack.neg"], [
         {
            "if": ["==", "simonSaysIndicator", 1],
            "then": [
              {"say": "ack.pos.correctsfx"},
              {"say": "ack.pos.kids.mad"},
              {"set": "state", "val": "correct"}
            ],
            "else":
              {"say": "ack.neg.nosimonsays"}
          }
        ],
        ["otherwise"], [
          {"say": "ack.neg.wrongresponse"},
          {"say": "command.listen"},
          {"set": "state", "val": "wrong"}
        ]
      ]}
    ]
  },
  """

state = State(topic="catch.say-makemadface")

condition = Condition()
condition.addConditional(operator=Operator.EQUAL,name="state",value="catch-say-makemadface")
state.setCondition(condition.get())

steps = Steps()
steps.add(step={"set": "system.timer", "val": 0})

ifthenelse = IfThenElse()
condition = Condition()
condition.addConditional(operator=Operator.EQUAL, name="simonSaysIndicator", value=1)
ifthenelse.setIf(if_part=condition.get())
ifthenelse.addThen(then_part={"say": "ack.pos.correctsfx"})
ifthenelse.addThen(then_part={"say": "ack.pos.kids.mad"},)
ifthenelse.addThen(then_part={"set": "state", "val": "correct"})
ifthenelse.addElse(else_part={"say": "ack.neg.nosimonsays"})

userSays.add(ifUserSays=["vision.angry", "ack.neg"], thenAgentResponds=ifthenelse.get())
userSays.add(ifUserSays=["otherwise"], thenAgentResponds={"say": "ack.neg.wrongresponse"})
userSays.add(ifUserSays=["otherwise"], thenAgentResponds={"say": "command.listen"})
userSays.add(ifUserSays=["otherwise"], thenAgentResponds={"set": "state", "val": "wrong"})
steps.add(step=userSays.get())

state.setSteps(steps=steps.get())
subdialogue.addState(state=state)

print(subdialogue.getJsonData())