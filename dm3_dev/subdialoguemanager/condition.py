class Condition:
    def __init__(self):
        self.condition = ["&&"]
    def add(self,aCondition):
        self.condition.append(aCondition)
    def addConditional(self,operator, name, value):
        self.condition.append([operator.value, name, value])
    def get(self):
        if len(self.condition) == 2:
            self.condition = self.condition[1]
        return self.condition