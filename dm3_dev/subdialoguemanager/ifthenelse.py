class IfThenElse:
    def __init__(self):
        self.ifthenelse={'if':None, 'then':[], 'else':[]}
    def setIf(self, if_part):
        self.ifthenelse['if'] = if_part
    def addThen(self, then_part):
        self.ifthenelse['then'].append(then_part)
    def addElse(self, else_part):
        self.ifthenelse['else'].append(else_part)
    def get(self):
        return self.ifthenelse