class UserSays:
    def __init__(self):
        self.userPaths=[]
    def add(self,ifUserSays,thenAgentResponds): #ifUserSays is a list
    	try:
    		ifUserSaysIndex = self.userPaths.index(ifUserSays)
    	except ValueError:
    		self.userPaths += [ifUserSays,[thenAgentResponds]]
    		return
    	self.userPaths[ifUserSaysIndex+1].append(thenAgentResponds)
    def get(self):
    	return {'userSays':self.userPaths}