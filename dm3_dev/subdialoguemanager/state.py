class State:
	def __init__(self,topic):
		self.topic = topic
		self.parallel = False
		self.condition = None
		self.steps = None
	def getTopic(self):
		return self.topic
	def setCondition(self,condition):
		self.condition = condition
	def getCondition(self):
		return self.condition
	def setSteps(self,steps):
		self.steps = steps
	def getSteps(self):
		return self.steps
	def parallelize(self):
		self.parallel = True
	def isParallelized(self):
		return self.parallel
