from enum import Enum
class Operator(Enum):
	AND = '&&'
	OR = '||'
	EQUAL = '=='
	GTE = '>='
	GT = '>'
	LT = '<'
	LTE = '<='