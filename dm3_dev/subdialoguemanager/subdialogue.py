import json
class Subdialogue:
    def __init__(self, name):
    	self.states=[]
    	self.name = name
    	self.data = {}
    def addState(self, state):
    	self.states.append(state)
    	self.data['topic'] = state.getTopic()
    	self.data['condition'] = state.getCondition()
    	self.data['steps'] = state.getSteps()
    def getStates(self):
    	return self.states
    def getName(self):
    	return self.name
    def getJsonData(self):
    	json_data = json.dumps(self.data)
    	return json_data