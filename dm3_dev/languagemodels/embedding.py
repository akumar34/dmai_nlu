import tensorflow as tf
import abc
from abc import ABCMeta, abstractmethod

import gensim
from gensim.test.utils import datapath, get_tmpfile
from gensim.models import KeyedVectors
from gensim.scripts.glove2word2vec import glove2word2vec

import os

from collections import defaultdict

import numpy as np

class Embedding(metaclass=ABCMeta):
    __metaclass__ = abc.ABCMeta

    def __init__(self,embedding_path):
        self.embedding_path = embedding_path
        self.word_to_index_dict = None
        self.index_to_embedding_array = None
        self.word_to_embedding_dict = None

        self.tf_embedding = None
        self.tf_embedding_placeholder = None
        self.tf_embedding_init = None

        self.build()

    def build(self):
        self.build_weights()
        self.build_tf_layer()

    @abc.abstractmethod
    def build_weights(self):
        pass

    def build_tf_layer(self):
        #Variable that will hold the embedding is tf_embedding (that is, the weight parameter)
        self.tf_embedding = tf.Variable(tf.constant(0.0, shape=self.index_to_embedding_array.shape),trainable=False,name="Embedding")
        #now setup a placeholder for the index 2 embedding dictionary, but only when the session is run
        self.tf_embedding_placeholder = tf.placeholder(tf.float32, shape=self.index_to_embedding_array.shape)
        #finally tf_embedding weight parameter will be assigned to the index 2 embedding dictionary when session is run
        #the assignment to the weight parameter will be executed on tf_embedding_init reference.
        self.tf_embedding_init = self.tf_embedding.assign(self.tf_embedding_placeholder)
        #let's return the tf_embedding now that we have it
        #return tf_embedding, tf_embedding_placeholder, tf_embedding_init

    def get_weights(self):
        return self.word_to_index_dict, self.index_to_embedding_array, self.word_to_embedding_dict

    def get_tf_layer(self):
        return self.tf_embedding, self.tf_embedding_placeholder, self.tf_embedding_init

class GloveEmbedding(Embedding):
    def __init__(self,embedding_path):
        super().__init__(embedding_path)

    def build_weights(self):
        #get file name
        embedding_file_name = os.path.basename(self.embedding_path)
        #gensim only uses word2vec format, so create temporary word2vec file that will later on populate with correct format of glove
        tmp_file = get_tmpfile('word2vec.' + ''.join(embedding_file_name.split('.')[1:]))
        #transform glove file to word2vec and store in tmp_file
        glove2word2vec(self.embedding_path, tmp_file)
        embedding_file_name = tmp_file
        #load the glove embedding from word2vec tmp_file
        embedding = KeyedVectors.load_word2vec_format(embedding_file_name)
        #for any words not found, just assign all zeros.
        _WORD_NOT_FOUND = [0.0]* len(embedding['the'])
        #create string word to integer index dictionary
        self.word_to_index_dict = {word:index for index,word in enumerate(embedding.wv.index2word)}
        _LAST_INDEX = len(embedding.wv.vocab)
        #add one more index to the dictionary as a placeholder, (the embedding vocab size + 1)
        self.word_to_index_dict = defaultdict(lambda: _LAST_INDEX, self.word_to_index_dict)
        #create the word index to word embedding array
        self.index_to_embedding_array = [embedding[word] for _,word in enumerate(embedding.wv.index2word)]
        #the final element in this array is all zeros to represent unknown words
        self.index_to_embedding_array = np.array(self.index_to_embedding_array + [_WORD_NOT_FOUND])
        #create word to embedding dictionary
        self.word_to_embedding_dict = {word:embedding.wv[word] for index,word in enumerate(embedding.wv.index2word)}
        #add one more element in dictionary corresponding to unkown word
        self.word_to_embedding_dict = defaultdict(lambda: _WORD_NOT_FOUND)
        #return the word to index dictionary and index to embedding array. Note: index_to_embedding_array[word_to_index_dict['hey']].
        #return the word to embedding dictionary. Note: word_to_embedding_dict['hey']
        #return word_to_index_dict, index_to_embedding_array, word_to_embedding_dict

class Word2VecEmbedding(Embedding):
    def __init__(self,embedding_path):
        super().__init__(embedding_path)

    def build_weights(self):
        #load the glove embedding from word2vec text file
        embedding = KeyedVectors.load_word2vec_format(self.embedding_path)
        #for any words not found, just assign all zeros.
        _WORD_NOT_FOUND = [0.0]* len(embedding['the'])
        #create string word to integer index dictionary
        self.word_to_index_dict = {word:index for index,word in enumerate(embedding.wv.index2word)}
        _LAST_INDEX = len(embedding.wv.vocab)
        #add one more index to the dictionary as a placeholder, (the embedding vocab size + 1)
        self.word_to_index_dict = defaultdict(lambda: _LAST_INDEX, self.word_to_index_dict)
        #create the word index to word embedding array
        self.index_to_embedding_array = [embedding[word] for _,word in enumerate(embedding.wv.index2word)]
        #the final element in this array is all zeros to represent unknown words
        self.index_to_embedding_array = np.array(self.index_to_embedding_array + [_WORD_NOT_FOUND])
        #create word to embedding dictionary
        self.word_to_embedding_dict = {word:embedding.wv[word] for index,word in enumerate(model.wv.index2word)}
        #add one more element in dictionary corresponding to unkown word
        self.word_to_embedding_dict = defaultdict(lambda: _WORD_NOT_FOUND)
        #return the word to index dictionary and index to embedding array. Note: index_to_embedding_array[word_to_index_dict['hey']].
        #return the word to embedding dictionary. Note: word_to_embedding_dict['hey']
        #return word_to_index_dict, index_to_embedding_array, word_to_embedding_dict
