import tensorflow as tf
import abc
from abc import ABCMeta, abstractmethod
from nltk import word_tokenize as wt

class NNLanguageModel(metaclass=ABCMeta):
	__metaclass__ = abc.ABCMeta

	def __init__(self, n_hidden, n_timesteps, batch_size, keep_prob, vocab_size):
		self.n_hidden = n_hidden
		self.n_timesteps = n_timesteps
		self.batch_size = batch_size
		self.keep_prob = keep_prob
		self.vocab_size = vocab_size

		self.outputs = None
		self.final_state = None


	@abc.abstractmethod
	def apply_forward_pass(self):
		pass

	def get_cell(self):
		#let us create the lstm next
		lstm = tf.nn.rnn_cell.BasicLSTMCell(self.n_hidden)
		#and dropout regularization
		drop = tf.nn.rnn_cell.DropoutWrapper(lstm, output_keep_prob=self.keep_prob)
		return drop

	def apply_softmax(self):
		# Reshape output so it's a bunch of rows, one row for each step for each sequence.
		# That is, the shape should be batch_size*num_steps rows by n_layers columns
		seq_output = tf.concat(self.outputs, axis=1)
		x = tf.reshape(seq_output, [-1, self.vocab_size])
	    
		# Connect the RNN outputs to a softmax layer
		with tf.variable_scope('softmax'):
			softmax_w = tf.Variable(tf.truncated_normal((self.n_hidden, self.vocab_size), stddev=0.1))
			softmax_b = tf.Variable(tf.zeros(self.vocab_size))
	    
		# Since output is a bunch of rows of RNN cell outputs, logits will be a bunch
		# of rows of logit outputs, one for each step and sequence
		logits = tf.matmul(x, softmax_w) + softmax_b
	    
		# Use softmax to get the probabilities for predicted characters
		output_probs = tf.nn.softmax(logits, name='predictions')
	    
	    #logits is vocab_size dimenions and logits = w*X + b, out = softmax(logits)
		return output_probs, logits

	def transform_to_features(self, data):
		#Reshape data to the dimensions (batch_size X n_steps) so that it can be fed into an LSTM

		#total number of words for each batch
		words_per_batch = self.batch_size * self.n_timesteps
		#total number of batches, each one of batch_size size
		n_batches = len(data)//words_per_batch
		#only consider full batches, since we cannot send in the leftover in the end to an LSTM
		data = data[:n_batches * words_per_batch]
		#reshape the (full batches) data into a single row
		data = data.reshape((self.batch_size,-1))
		#iterate every n_timesteps and generate the corresponding (shifted by 1) labels
		for i in range(0, data.shape[1], self.n_timesteps):
			#the features are the word unigrams of the sentences, every n_timesteps intervals
			x = data[:, i:i+self.n_timesteps]
			#the labels are the data shifted by 1; ex: input: [0 to 9] then labels: [1 to 10]
			y_temp = data[:, i+1:i+self.n_timesteps+1]
		    # For the very last batch, y will be one word short at the end of 
		    # the sequences which breaks things. To get around this, make an
		    # array of the appropriate size first, of all zeros, then add the targets.
		    # This will introduce a small artifact in the last batch, but it won't matter.
			y = np.zeros(x.shape, dtype=x.dtype)
			y[:,:y_temp.shape[1]] = y_temp

			yield x,y

class LSTM(NNLanguageModel):
	def __init__(self, data, n_layers, n_hidden, n_timesteps, batch_size, keep_prob, vocab_size):
		self.data = data
		self.n_layers = n_layers
		super().__init__(data, n_hidden, n_timesteps, batch_size, keep_prob, vocab_size)

	def apply_forward_pass(self):
		#make it multi-layered LSTM
		cell = get_cell(data)
		#getting an initial state of all zeros (dimensions n_hidden X 1)
		initial_state = cell.zero_state(self.batch_size, tf.float32)
		#the forward pass to the RNN has outputs (cell memory) and final_state
		self.outputs, self.final_state = tf.nn.dynamic_rnn(cell, self.data, initial_state=initial_state)

class MultiLayeredLSTM(NNLanguageModel):
	def __init__(self, data, n_layers, n_hidden, n_timesteps, batch_size, keep_prob, vocab_size):
		self.data = data
		self.n_layers = n_layers
		super().__init__(n_hidden, n_timesteps, batch_size, keep_prob, vocab_size)
    	
	def apply_forward_pass(self):
		#make it multi-layered LSTM
		cell = tf.nn.rnn_cell.MultiRNNCell([self.get_cell() for _ in range(self.n_layers)])
		#getting an initial state of all zeros. Remember that 
		initial_state = cell.zero_state(self.batch_size, tf.float32)
		#the forward pass to the RNN
		self.outputs, self.final_state = tf.nn.dynamic_rnn(cell, self.data, initial_state=initial_state)