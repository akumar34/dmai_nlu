import json
import sys

def stats(data):
    counter = {
        'cam': set(),
        'obj': set(),
        'pic': set()
    }
    
    for datum in data:
        steps = datum['steps']
        for step in steps:
            showobj = step['show']
            for k, v in showobj.items():
                counter[k].add(v)

    for datatype, examples in counter.items():
        sys.stdout.write(datatype)
        sys.stdout.write('\n')
        for example in sorted(list(examples)):
            sys.stdout.write(example)
            sys.stdout.write('\n')
        sys.stdout.write('\n\n')
    sys.stdout.flush()




    
if __name__ == '__main__':
    data = None
    with open('SimonSaysContent.json') as f:
        data = json.load(f)
    if data is None:
        print('Error loading json file')
        exit(1)
    stats(data)
