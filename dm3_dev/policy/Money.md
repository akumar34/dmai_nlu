J1: Pennies and nickels

> say "Which coin is this?"
> show [{name: penny, attr: back}]
    CA: Penny
    WA: Nickel
    SCA: It's brown
    IDK: I don't know

J2: Dimes and quarters

> say "Which coin is this?"
> show [{name: dime, attr: front}]
    CA: Dime
    WA: Quarter
    SCA: It's heads
    IDK

J3: Pinnies, nickels, dimes, and quarters

> say "Which coin is this?"
> show [{name: penny, attr: front}]
    CA: Penny
    WA: Nickel
    SCA: It's brown
    IDK

J4: Count pennies

> say "How much money is there?"
> show [{name: penny}, {name: penny}, {name: penny}, {name: penny}, {name: penny}, {name: penny}, {name: penny}, {name: penny}]
    CA: 8
    WA: 9
    SCA: pennies
    IDK

T2: Coin values - penny through quarter

> say "How much is this coin worth?"
> show [{name: penny, attr: back}]
    CA: 1 cent
    WA: 5 cents
    SCA: it's a penny
    IDK

T4: Count money - pennies and nickels

> say "How much money is there?"
> show [{name: penny}, {name: nickel}]
    CA: 6 cents
    WA: 5 cents
    SCA: a nickel and a penny
    IDK

T5: Count money - pennies through dimes

> say "How much money is there?"
> show [{name: nickel}, {name: penny}, {name: nickel}, {name: nickel}, {name: nickel}]
    CA: 21 cents
    WA: 20 cents
    SCA: nickels and pennies
    IDK

T6: Equivalent groups of coins - pennies through dimes

> say "Look at these coins"
> show [{items: [{name: dime}, {name: dime}, {name: dime}, {name: dime}, {name: nickel}, {name: penny}, {name: penny}, {name: penny}]}]
> say "Does this group show the same amount?"
> show [{items: [{name: dime}, {name: dime}, {name: dime}, {name: dime}, {name: nickel}, {name: penny}, {name: penny}, {name: penny}]},
        {items: [{name: dime}, {name: dime}, {name: dime}, {name: nickel}, {name: penny}, {name: penny}, {name: penny}]}]
    CA: yes
    WA: no
    SCA: they look different
    IDK

T7: Exchanging groups of coins - pennies through dimes

> say "How many nickels can you get for the pennies?"
> show [{name: penny}, {name: penny}, {name: penny}, {name: penny}, {name: penny}]
    CA: 1 nickel
    WA: none
    SCA: 5 cents
    IDK

T8: Exchanging coins - pennies through dimes

> say "How many dimes is this?"
> show [{name: penny, label: 10}]
    CA: 1
    WA: 2
    SCA: 10 cents
    IDK

T9: Compare two groups of coins - pennies through dimes

> say "Which is more?"
> show [{order: 1, items: [{name: penny}, {name: nickel}]},
        {order: 2, items: [{name: dime}]}]
    CA: The first one
    WA: the second one
    SCA: they're different
    IDK