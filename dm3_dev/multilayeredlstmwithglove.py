from languagemodels import *
import tensorflow as tf
import numpy as np

#Load GloVe embedding weights to the appropriate index dictionaries and arrays
gloveEmbedding = GloveEmbedding(embedding_path='languagemodels/glove.6B/glove.6B.50d.txt')
word_to_index, index_to_embedding, _ = gloveEmbedding.get_weights()
vocab_size, embedding_dim = index_to_embedding.shape

#load training data and transform to corresponding word indices
training_file = 'test.txt'
with open(training_file,'r') as f:
    text = f.read()
encoded = np.array([word_to_index[word] for word in text], dtype=np.int32)

#add input and label placeholders. Dimensions are batch_size X n_timesteps (Currently None since we do not know them yet)
inputs = tf.placeholder(tf.int32, [None, None], name='inputs')
labels = tf.placeholder(tf.int32, [None, None], name='labels')
#tf_embedding is the weight matrix for our embedding layer
tf_embedding, tf_embedding_placeholder, tf_embedding_init = gloveEmbedding.get_tf_layer()
#the actual embedding layer is defined with the tf_embedding variable (that is, the weight parameter) and the input word indices
embedding_word_ids = tf.nn.embedding_lookup(tf_embedding, inputs)
#use for dropout
keep_prob = tf.placeholder(tf.float32, name='keep_prob')
#LSTM parameters
n_layers = 2
n_hidden = 50
n_timesteps = 20
batch_size = 50
keep_prob = 0.5
#the RNN model will give us the predicted output (that is, this is a forward pass of the neural network)
#the forward pass to the RNN, and remember that the input to the RNN is the output of the embedding layer
multi_layered_lstm = MultiLayeredLSTM(data = embedding_word_ids, n_layers = n_layers, n_hidden = n_hidden, n_timesteps = n_timesteps, batch_size = batch_size, keep_prob = keep_prob, vocab_size = vocab_size)
#compute the value of the final hidden state, applying forward pass for the current weights
multi_layered_lstm.apply_forward_pass()
#apply forward pass to compute softmax(weight * final state + bias)
output_probs, logits = multi_layered_lstm.apply_softmax()

init_op = tf.global_variables_initializer()

with tf.Session() as session:
    session.run(init_op)
    #run session on tf_embedding_unit so that it can be assigned the index 2 embedding. 
    #now we have the full tf_embedding in tensorflow loaded with GloVe!
    _ = session.run(tf_embedding_init, feed_dict={tf_embedding_placeholder: index_to_embedding})