import time
import os
import argparse
import jsoncompare
from jsoncompare import Diff

parser = argparse.ArgumentParser('subdialogue_graph_generator')
parser.add_argument('-gc', nargs='+', help='Generate and compare a list of subdialogues: subdialogue1.json subdialogue2.json, ...')
parser.add_argument('-c', nargs='+', help='Compare a list of node-link-value json files: nlv_subdialogue1.json, nlv_subdialogue2.json, ...')
parser.add_argument('-g', nargs='+', help='Generate a list of subdialogues: subdialogue1.json, subdialogue2.json, ...')
args = parser.parse_args()

mode = ''
subdialogues = []
for key, value in parser.parse_args()._get_kwargs():
    if value == None:
    	continue

    mode = key
    subdialogues = value
    break

import json

def isStateTransition(rule):
	return 'set' in rule and 'val' in rule and rule['set'] == 'state'

def getNextState(step):
	next_state_paths = []
	for step_name, step_value in step.items():
		if step_name == 'set':
			continue
		if isStateTransition(step):
			next_state_paths.append(step['val'])
		elif type(step_value) == dict:
			if isStateTransition(step_value):
				next_state_paths.append(step_value['val'])
		elif type(step_value) == list:
			for container in step_value:
				if type(container) != list:
					continue
				for item in container:
					if type(item) != dict:
						continue
					if isStateTransition(item):
						next_state_paths.append(item['val'])
					if 'then' in item or 'else' in item:
						for _, element_value in item.items():
							for aElement_value in element_value:
								if type(aElement_value) != dict:
									continue
								if isStateTransition(aElement_value):
									next_state_paths.append(aElement_value['val'])
	return next_state_paths

def get_source(condition,topic):
	for index in range(0, len(condition)):
		aCondition = condition[index]

		if type(aCondition) == int:
			continue

		if 'state' not in aCondition:
			continue

		if type(aCondition) == list:
			return aCondition[aCondition.index('state') + 1]

		else:
			return condition[index+1]

	return topic

def orderJson(obj):
	if isinstance(obj, dict):
		return sorted((k, orderJson(v)) for k, v in obj.items())
	if isinstance(obj, list):
		return sorted(orderJson(x) for x in obj)
	else:
		return obj

import itertools

def generateSubsets(arr, length):
 
    # return list of all subsets of length r
    # to deal with duplicate subsets use 
    # set(list(combinations(arr, r)))
    return list(itertools.combinations(arr, length))
 
#import sys
#if len(sys.argv) == 1:
#	INPUT_SUBDIALOGUE = '../../dm3/policy/SimonSaysRoundM.json'
#else:
#	INPUT_SUBDIALOGUE = sys.argv[1]

if 'g' in mode:
	for subdialogue in subdialogues:
		print("PROCESSING FOR SUBDIALOGUE " + str(subdialogue))
		with open(subdialogue) as json_data:
			data = json.load(json_data)

		nodes_file_name = 'nodes_' + os.path.basename(subdialogue)[:-1*len('.json')] + '.csv'
		if os.path.isfile(nodes_file_name):
			os.remove(nodes_file_name)
		nodes_f = open(nodes_file_name,'a')
		output = 'id,name,Group\n'
		nodes_f.write(output)
		nodes = dict()

		edges_file_name = 'edges_' + os.path.basename(subdialogue)[:-1*len('.json')] + '.csv'
		if os.path.isfile(edges_file_name):
			os.remove(edges_file_name)
		edges_f = open(edges_file_name,'a')
		output = 'source,target,value\n'
		edges_f.write(output)
		edges = dict()

		unique_nodes = []
		for state in data:
			condition = state['condition']
			topic = state['topic']
			source = get_source(condition,topic)
			
			steps = state['steps']

			for step in steps:
				targets = getNextState(step)

				if len(targets) == 0:
					continue

				for target in targets:

					if source not in unique_nodes:
						unique_nodes.append((source,topic))

					if target not in unique_nodes:
						unique_nodes.append((target,topic))

					if source not in edges:
						edges[source] = []
					
					edges[source].append((target,'"' + str(condition) + '"'))

		node_id = 0
		for node in unique_nodes:
			nodes[node[0]] = node_id
			output = str(node[0]) + "," + str(node[0]) + "," + str(node[1]) + '\n'
			#output = str(node[0]) + "," + str(node[0]) + "," + str(3) + '\n'
			nodes_f.write(output)

			node_id += 1

		for source,targets in edges.items():
			for target in targets:
				output = str(source) + ',' + str(target[0]) + ',' + str(target[1]) + '\n'
				#output = str(source) + ',' + str(target[0]) + ',' + str(2) + '\n'
				edges_f.write(output)

		nodes_f.close()
		edges_f.close()

		import networkx as nx
		import pandas as pd
		from matplotlib.pylab import plt
		from networkx.readwrite import json_graph
		#matplotlib inline
		 
		G = nx.DiGraph()
		 
		# Read csv for nodes and edges using pandas:
		nodes = pd.read_csv(nodes_file_name)
		edges = pd.read_csv(edges_file_name)
		 
		# Dataframe to list:
		nodes_list = nodes.values.tolist()
		edges_list = edges.values.tolist()
		 
		# Import id, name, and group into node of Networkx:
		for i in nodes_list:
		    G.add_node(i[0], name=i[1], group=i[2])
		 
		# Import source, target, and value into edges of Networkx:
		for i in edges_list:
		    G.add_edge(i[0],i[1], value=i[2])
		 
		# Write json for nodes-links format:
		import json
		j = json_graph.node_link_data(G)

		#for el in j['nodes']:
		#	del el['name']
		nodes_link_value_file = 'node-link-value.json'
		js = json.dumps(j, ensure_ascii=False, indent=2)
		with open(nodes_link_value_file, "w") as file:
		     file.write(js)

		nlv_subdialogue_file = 'nlv_' + os.path.basename(subdialogue)
		with open(nlv_subdialogue_file, "w") as file:
		     file.write(js)

		import subprocess
		import shlex
		cmd = 'firefox subdialogue_graph.html'
		formatted_cmd = shlex.split(cmd)
		subprocess.Popen(formatted_cmd)

		# Visualize the network:
		#nx.draw_networkx(G, node_size=2000, font_size=8, with_labels=True)
		#nx.draw_spring(G, node_size=2000, font_size=8, with_labels=True)
		#plt.show()

		time.sleep(5)

if 'c' in mode:
	subdialogues_data = []

	for subdialogue in subdialogues:
		nlv_subdialogue_file = 'nlv_' + os.path.basename(subdialogue)
		with open(nlv_subdialogue_file,'r') as f:
			subdialogues_data.append(orderJson(json.load(f)))

	subdialogue_pairs = generateSubsets(subdialogues_data, 2)

	for subdialogue1, subdialogue2 in subdialogue_pairs:
		if subdialogue1 == subdialogue2:
			print("matched!")
		else:
			print("did not match!")