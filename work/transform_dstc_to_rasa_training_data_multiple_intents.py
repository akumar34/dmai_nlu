import json
from os import walk
from nltk.tokenize import word_tokenize as wt
import re

def matching(s,pattern):
	prog = re.compile(pattern)
	result = prog.match(s)
	return result

def load_training_data(input_path):
	user_intents = set()
	user_entities = set()
	system_templates = dict()
	user_templates = dict()
	user_slots = dict()

	annotations = dict()
	annotations_map = dict()
	relevant_files = []

	for(dirpath, dirnames, filenames) in walk(input_path):
		for filename in filenames:
			relevant_files.append(dirpath + '/' + filename)

	for i in range(0,len(relevant_files),2):
		with open(relevant_files[i],'rt') as label_file:
			label_data = json.loads(label_file.read())
		
		with open(relevant_files[i+1],'rt') as log_file:
			log_data = json.loads(log_file.read())

		example_tokens = relevant_files[i].split('/')
		example_name = example_tokens[len(example_tokens)-2]

		if example_name not in annotations:
			annotations[example_name] = []

		total_turns = len(label_data['turns'])

		for turn_index in range(0,total_turns):
			log_turn = log_data['turns'][turn_index]
			#text = relevant_files[i+1] + '\t' + log_turn['output']['transcript']
			text = log_turn['output']['transcript']
			processed_text = text

			for semantics in log_turn['output']['dialog-acts']:
				slots = semantics['slots']

				for slot in slots:
					entity = slot[0]
					value = slot[1]
					start = processed_text.lower().find(value)
					#end = start + len(value)
					end = start + len(wt(processed_text[start:])[0])

					if entity != 'slot' and log_turn['output']['aborted'] == False and start != -1:
						processed_text = processed_text[:start] + '{' + entity + '}' + processed_text[end:]

			for semantics in log_turn['output']['dialog-acts']:
				act = semantics['act']
				slots = semantics['slots']

				entities = []
				for slot in slots:
					slot_map = dict()
					entity = slot[0]
					value = slot[1]
					start = processed_text.lower().find(value)
					#end = start + len(value)
					end = start + len(wt(processed_text[start:])[0])

					slot_map['entity']=entity
					slot_map['value']=value
					slot_map['start']=start
					slot_map['end']=end

					entities.append(slot_map)

					if entity not in user_slots:
						user_slots[entity] = set()
					user_slots[entity].add(value)

				if act not in annotations_map:
					annotations_map[act] = []

				annotations_map[act].append(('SYSTEM',text,processed_text, act,entities))
				annotations[example_name].append(('SYSTEM',text,processed_text,act,entities))

				if act not in system_templates:
					system_templates[act] = set()

				#system_templates[act].add((text, processed_text))
				system_templates[act].add(processed_text)


			label_turn = label_data['turns'][turn_index]
			#voip-0a45bc863d-20130325_200515
			#text = relevant_files[i] + '\t' + label_turn['transcription']
			text = label_turn['transcription']
			processed_text = text

			'''for semantics in label_turn['semantics']['json']:
				slots = semantics['slots']

				for slot in slots:
					entity = slot[0]
					value = slot[1]
					start = processed_text.lower().find(value)
					end = start + len(value)

					if entity != 'slot' and start != -1:
						processed_text = processed_text[:start] + '{' + entity + '}' + processed_text[end:]'''

			semantics = label_turn['semantics']
			act_tokens = semantics['cam'].split('|')
			acts = []
			for act_token in act_tokens:
				parsed_act = matching(act_token,'[a-z]*').group(0)
				acts.append(parsed_act)
			acts = sorted(acts)

			act = ''
			for act_token in acts:
				act += act_token + '_'
			act = act[:-1]

			for semantic in semantics['json']:
				#act = semantic['act']
				slots = semantic['slots']

				entities = []
				for slot in slots:
					slot_map = dict()
					entity = slot[0]
					value = slot[1]
					start = processed_text.lower().find(value)
					#end = start + len(value)
					end = start + len(wt(processed_text[start:])[0])

					slot_map['entity']=entity
					slot_map['value']=value
					slot_map['start']=start
					slot_map['end']=end

					entities.append(slot_map)
					user_entities.add(entity)

				if act not in annotations_map:
					annotations_map[act] = []

				annotations_map[act].append(('USER',text,processed_text, act,entities))
				annotations[example_name].append(('USER',text,processed_text, act,entities))

				user_intents.add(act)

				if act not in user_templates:
					user_templates[act] = set()

				user_templates[act].add(processed_text)

	return annotations_map,annotations, user_intents, user_entities, user_slots, system_templates, user_templates

def build_rasanlu_formatted_training_data(annotations_map):
	data = dict()
	data['rasa_nlu_data'] = dict()
	data['rasa_nlu_data']['regex_features'] = []
	data['rasa_nlu_data']['entity_synonyms'] = []
	data['rasa_nlu_data']['common_examples'] = []
	for act,dialog_acts in annotations_map.items():
		for dialog_act in dialog_acts:
			source,text,_,_,slots = dialog_act
			if source == 'SYSTEM':
				continue

			result = dict()
			result['text'] = text
			result['intent'] = act
			result['entities'] = slots
			data['rasa_nlu_data']['common_examples'].append(result)

	return json.loads(json.dumps(data, ensure_ascii=False))

def build_domain_string(user_intents, user_entities, user_slots, system_templates, user_templates):
	domain = 'intents:\n'

	for intent in user_intents:
		domain += '  - ' + intent + '\n'

	domain += '\nentities:\n'
	for entity in user_entities:
		domain += '  - ' + entity + '\n'

	domain += '\ntemplates:\n'
	for action,utterances in system_templates.items():
		domain += '  ' + action + ':\n'
		for utterance in utterances:
			#domain += '    - ' + utterance[1] + '\n'
			domain += '    - ' + utterance + '\n'			
		domain += '\n'

	domain += 'actions:\n'
	for action in system_templates.keys():
		domain += '  - ' + action + '\n'

	domain += '\nslots:\n'
	for slot_name, slot_value in user_slots.items():
		domain += '  ' + slot_name + ':\n'
		domain += '    type: text\n'

	domain += '\nuser templates:\n'
	for action,utterances in user_templates.items():
		domain += '  ' + action + ':\n'
		for utterance in utterances:
			#domain += '    - ' + utterance[1] + '\n'
			domain += '    - ' + utterance + '\n'			
		domain += '\n'
	return domain

input_path = 'dstc2_traindev/data/'
annotations_map, annotations, user_intents, user_entities, user_slots, system_templates, user_templates = load_training_data(input_path)

output_path = 'training_data_for_dstc2_multiple_intents.json'
rasanlu_trainingdata = build_rasanlu_formatted_training_data(annotations_map)
with open(output_path,'w') as outfile:
	json.dump(rasanlu_trainingdata,outfile)

output_path = 'dstc2_training_summary_multiple_intents.yml'
domain = build_domain_string(user_intents, user_entities, user_slots, system_templates, user_templates)
with open(output_path,'w') as outfile:
	outfile.write(domain)