from rasa_nlu.model import Metadata, Interpreter
from rasa_nlu import config

model_directory='models/dstc2/tensorflow_embedding_multiple_intents/default/model_20180523-184720/'
interpreter = Interpreter.load(model_directory)

print("Model is loaded...",interpreter)
result = interpreter.parse('can you give me some recommendations on cheap indian restaurants')
print(result)