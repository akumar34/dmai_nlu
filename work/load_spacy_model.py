from rasa_nlu.model import Metadata, Interpreter
from rasa_nlu import config

model_directory='models/dstc2/spacy/default/model_20180530-165245/'
interpreter = Interpreter.load(model_directory)

print("Model is loaded...",interpreter)
result = interpreter.parse('can you give me some recommendations on cheap indian restaurants')
print(result)