'''from rasa_nlu.training_data import load_data
from rasa_nlu.config import RasaNLUModelConfig
from rasa_nlu.model import Trainer
from rasa_nlu import config

builder = ComponentBuilder(use_cache=True)

training_data = load_data('training_data_for_nlu.json')
trainer = Trainer(config.load('config_spacy.yml'))
trainer.train(training_data)
model_directory = trainer.persist('models/nlu/', fixed_model_name='current')'''
import pickle
model_directory = None
try:
	model_directory = pickle.load(open('model_directory_tensorflow_embedding.pkl','rb'))
except:
	print('No model exists')

from rasa_nlu.components import ComponentBuilder
builder = ComponentBuilder(use_cache=True)      # will cache components between pipelines (where possible)

if model_directory == None:
	from rasa_nlu.training_data import load_data
	from rasa_nlu import config
	from rasa_nlu.components import ComponentBuilder
	from rasa_nlu.model import Trainer

	training_data = load_data('training_data_for_dstc2.json')
	trainer = Trainer(config.load("config_tensorflow_embedding.yml"), builder)
	trainer.train(training_data)
	model_directory = trainer.persist('models/dstc2/tensorflow_embedding/')

	pickle.dump(model_directory,open('model_directory_tensorflow_embedding.pkl','wb'))
	pickle.dump(training_data,open('training_data_tensorflow_embedding.pkl','wb'))

from rasa_nlu.model import Metadata, Interpreter
from rasa_nlu import config

interpreter = Interpreter.load(model_directory, builder)