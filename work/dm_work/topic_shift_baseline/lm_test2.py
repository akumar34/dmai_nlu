from languagemodel import *

import tensorflow as tf
from nltk import word_tokenize
import numpy as np

tf.reset_default_graph()

training_file = 'test.txt'
with open(training_file,'r') as f:
	training_text = f.read()

embedding_config = {'model_name':None,'embedding_path':'glove.6B/word2vec.6B.50d.txt', 'embedding_dimensions':50}
rnn_model_config = {'model_name':'MultiLayeredLSTM','n_layers':2,'n_hidden':512,'batch_size':20}
language_model_config = {'learning_rate':0.001, 'keep_prob':0.5, 'gradient_clip':5, 'n_timesteps':3}

sentence_generation = SentenceGeneration(
						training_text=training_text, 
						embedding_config=embedding_config, 
						rnn_model_config=rnn_model_config, 
						language_model_config=language_model_config)

trained_weights = tf.train.latest_checkpoint('multilayeredlstm_checkpoints')

sentence_generation.prepare_models_for_generation()

next_words, predictions = sentence_generation.generate('agree that is',5, trained_weights)
print(next_words, predictions)
next_words, predictions = sentence_generation.generate('the agree that', 5, trained_weights)
print(next_words, predictions)
next_words, predictions = sentence_generation.generate('that is',5, trained_weights)
print(next_words, predictions)

'''def pick_top_n(preds, vocab_size, top_n=5):
	p = np.squeeze(preds)
	#zero out all the probability values that are less than the top n values
	p[np.argsort(p)[:-top_n]] = 0
	#compute the average for the top n predictions (remaining are zero)
	p = p / np.sum(p)
	#draw a word from this distribution p
	word_idx = np.random.choice(vocab_size, 1, p=p)[0]
	return word_idx, p[word_idx]

def generate(given_sentence, n_next_words, rnn_model, language_model, trained_weights, word_to_index, index_to_word):
	#Generating the next word given the current word requires the models to accept only one word at a time
	next_words = word_tokenize(given_sentence)
	predictions = []
	saver = tf.train.Saver()
	with tf.Session() as session:
		saver.restore(session, trained_weights)
		
		new_state = session.run(rnn_model.initial_state)

		for given_word in next_words:
			x = np.zeros((1, 1))
			x[0,0] = word_to_index[given_word]
			feed = {language_model.X: x,rnn_model.keep_prob: 1.,rnn_model.initial_state: new_state}
			preds, new_state = session.run([language_model.prediction, rnn_model.final_state], feed_dict=feed)

		word_idx, pick_pred = pick_top_n(preds, language_model.vocab_size, top_n=5)
		next_words.append(index_to_word[word_idx])
		predictions.append(pick_pred)

		for i in range(n_next_words):
			x[0,0] = word_idx
			feed = {language_model.X: x,rnn_model.keep_prob: 1.,rnn_model.initial_state: new_state}
			preds, new_state = session.run([language_model.prediction, rnn_model.final_state], feed_dict=feed)

			word_idx, pick_pred = pick_top_n(preds, language_model.vocab_size)
			next_words.append(index_to_word[word_idx])
			predictions.append(pick_pred)

	return ' '.join(next_words), predictions

tf.reset_default_graph()

training_file = 'test.txt'
with open(training_file,'r') as f:
	training_text = f.read()'''

'''embedding_config = {'model_name':None,'embedding_path':'glove.6B/glove.6B.50d.txt', 'embedding_dimensions':50}
rnn_model_config = {'model_name':'MultiLayeredLSTM','n_layers':2,'n_hidden':512,'batch_size':20}
language_model_config = {'learning_rate':0.001, 'keep_prob':0.5, 'gradient_clip':5, 'n_timesteps':3}

sentence_generation = SentenceGeneration(
						training_text=training_text, 
						embedding_config=embedding_config, 
						rnn_model_config=rnn_model_config, 
						language_model_config=language_model_config)

sentence_generation.train(epochs=20,print_every_n=1,save_every_n=5)

#Finished training language model for sentence generation! Now you can generate new sentences!

#language_model = sentence_generation.get_language_model()
#trained_weights = language_model.get_trained_weights()
#trained_weights_name = language_model.get_trained_weights_name()

#trained_weights_name = 'multilayeredlstm'
#trained_weights = tf.train.latest_checkpoint(trained_weights_name + "_checkpoints")
#word_to_index = sentence_generation.get_word_to_index()
#index_to_word = sentence_generation.get_index_to_word()

#tf.reset_default_graph()

embedding_config = {'model_name':None,'embedding_path':'glove.6B/glove.6B.50d.txt', 'embedding_dimensions':50}
rnn_model_config = {'model_name':'MultiLayeredLSTM','n_layers':2,'n_hidden':512,'batch_size':1}
language_model_config = {'learning_rate':0.001, 'keep_prob':0.5, 'gradient_clip':5, 'n_timesteps':1}

sentence_generation = SentenceGeneration(
						training_text=training_text, 
						embedding_config=embedding_config, 
						rnn_model_config=rnn_model_config, 
						language_model_config=language_model_config)

rnn_model = sentence_generation.get_rnn_model()
language_model = sentence_generation.get_language_model()
trained_weights_name = 'multilayeredlstm'
trained_weights = tf.train.latest_checkpoint(trained_weights_name + "_checkpoints")
word_to_index = sentence_generation.get_word_to_index()
index_to_word = sentence_generation.get_index_to_word()

next_words, predictions = generate('agree that is',5,rnn_model, language_model, trained_weights, word_to_index, index_to_word)
print(str(trained_weights_name))
print(next_words, predictions)'''