from embedding import GloveEmbedding
from embedding import Word2VecEmbedding
from rnn import MultiLayeredLSTM
from rnn import LSTM
from languagemodel import LanguageModel

import tensorflow as tf
import numpy as np
import time
from nltk import word_tokenize

class SentenceGeneration:
	def __init__(self, training_text, embedding_config, rnn_model_config, language_model_config):
		self.training_text = training_text
		self.embedding_config = embedding_config
		self.rnn_model_config = rnn_model_config
		self.language_model_config = language_model_config

		self.word_to_index = None
		self.index_to_word = None
		self.vocab_size = None

		self.rnn_model, self.language_model = self.build_output(
												batch_size=self.rnn_model_config['batch_size'], 
												n_timesteps=self.language_model_config['n_timesteps'])

	def get_word_to_index(self):
		return self.word_to_index

	def build_output(self, batch_size, n_timesteps):
		#tf.reset_default_graph()

		X = tf.placeholder(tf.int32, [batch_size, n_timesteps])
		y = tf.placeholder(tf.int32, [batch_size, n_timesteps])
		keep_prob = tf.placeholder(tf.float32)

		if self.embedding_config['model_name'] == None:
			vocab = sorted(word_tokenize(training_text))
			self.word_to_index = {word: i for i, word in enumerate(vocab)}	
			self.index_to_word = dict(enumerate(vocab))
			self.vocab_size = len(vocab)
			input_to_rnn = tf.one_hot(X, self.vocab_size)
		#TODO: elif: self.embedding_config.model_name == 'internal':
		else:
			if 'GloVe' in self.embedding_config['model_name']:
				embedding = GloveEmbedding(X=X, embedding_path=self.embedding_config['embedding_path'])
			elif 'Word2Vec' in self.embedding_config['model_name']:
				embedding = Word2VecEmbedding(X=X, embedding_path=self.embedding_config['embedding_path'])
			embedding_look_up, self.vocab_size, embedding_dimensions, self.word_to_index, self.index_to_word = embedding.train()
			input_to_rnn = embedding_look_up

		encoding = np.array([self.word_to_index[word] for word in word_tokenize(self.training_text)], dtype=np.int32)

		if 'MultiLayeredLSTM' in self.rnn_model_config['model_name']:
			rnn_model = MultiLayeredLSTM(
				X=input_to_rnn, 
				n_layers=self.rnn_model_config['n_layers'], 
				n_hidden=self.rnn_model_config['n_hidden'], 
				batch_size=batch_size, 
				keep_prob=keep_prob)
		elif 'LSTM' in self.rnn_model_config['model_name']:
			rnn_model = LSTM(
				X=input_to_rnn, 
				n_hidden=self.rnn_model_config['n_hidden'],
				batch_size=batch_size, 
				keep_prob=keep_prob)
		#TODO: elif self.rnn_model_config == 'BiDirectionalLSTM'
		#TODO: elif self.rnn_model_config == 'Seq2Seq'

		language_model = LanguageModel(
			rnn_model=rnn_model, 
			X=X, 
			y=y, 
			encoding=encoding, 
			n_timesteps=n_timesteps,
			vocab_size=self.vocab_size, 
			keep_prob=self.language_model_config['keep_prob'],
			learning_rate=self.language_model_config['learning_rate'], 
			gradient_clip=self.language_model_config['gradient_clip'])

		return rnn_model, language_model

	def get_rnn_model(self):
		return self.rnn_model

	def get_language_model(self):
		return self.language_model

	def train(self,epochs,print_every_n,save_every_n):
		trained_weights_name = self.rnn_model.name
		if self.embedding_config['model_name'] != None:
			trained_weights_name += '_' + self.embedding_config['model_name'] + '_' + str(self.embedding_config['embedding_dimensions'])

		self.language_model.train(
			epochs=epochs,
			print_every_n=print_every_n,
			save_every_n=save_every_n,
			trained_weights_name=trained_weights_name)

training_file = 'test.txt'
with open(training_file,'r') as f:
	training_text = f.read()

embedding_config = {'model_name':'GloVe','embedding_path':'glove.6B/glove.6B.50d.txt', 'embedding_dimensions':50}
rnn_model_config = {'model_name':'MultiLayeredLSTM','n_layers':2,'n_hidden':512,'batch_size':20}
language_model_config = {'learning_rate':0.001, 'keep_prob':0.5, 'gradient_clip':5, 'n_timesteps':3}

X = tf.placeholder(tf.int32, [rnn_model_config['batch_size'], language_model_config['n_timesteps']])
y = tf.placeholder(tf.int32, [rnn_model_config['batch_size'], language_model_config['n_timesteps']])
keep_prob = tf.placeholder(tf.float32)

if embedding_config['model_name'] == None:
	vocab = sorted(word_tokenize(training_text))
	word_to_index = {word: i for i, word in enumerate(vocab)}	
	index_to_word = dict(enumerate(vocab))
	vocab_size = len(vocab)
	input_to_rnn = tf.one_hot(X, vocab_size)
#TODO: elif: self.embedding_config.model_name == 'internal':
else:
	if 'GloVe' in embedding_config['model_name']:
		embedding = GloveEmbedding(X=X, embedding_path=embedding_config['embedding_path'])
	elif 'Word2Vec' in embedding_config['model_name']:
		embedding = Word2VecEmbedding(X=X, embedding_path=embedding_config['embedding_path'])
	embedding_look_up, vocab_size, embedding_dimensions, word_to_index, index_to_word = embedding.train()
	input_to_rnn = embedding_look_up

encoding = np.array([word_to_index[word] for word in word_tokenize(training_text)], dtype=np.int32)

if 'MultiLayeredLSTM' in rnn_model_config['model_name']:
	rnn_model = MultiLayeredLSTM(
		X=input_to_rnn, 
		n_layers=rnn_model_config['n_layers'], 
		n_hidden=rnn_model_config['n_hidden'], 
		batch_size=rnn_model_config['batch_size'], 
		keep_prob=keep_prob)
elif 'LSTM' in rnn_model_config['model_name']:
	rnn_model = LSTM(
		X=input_to_rnn, 
		n_hidden=rnn_model_config['n_hidden'],
		batch_size=rnn_model_config['batch_size'], 
		keep_prob=keep_prob)

language_model = LanguageModel(
	rnn_model=rnn_model, X=X, y=y, encoding=encoding, n_timesteps=language_model_config['n_timesteps'],
	vocab_size=vocab_size, keep_prob=language_model_config['keep_prob'], learning_rate=language_model_config['learning_rate'], gradient_clip=language_model_config['gradient_clip'])

trained_weights_name = rnn_model.name + '_GloVe_50'
language_model.train(epochs=20,print_every_n=1,save_every_n=10,trained_weights_name=trained_weights_name)
trained_weights = language_model.trained_weights

tf.reset_default_graph()

embedding_config = {'model_name':'GloVe','embedding_path':'glove.6B/glove.6B.50d.txt', 'embedding_dimensions':50}
rnn_model_config = {'model_name':'MultiLayeredLSTM','n_layers':2,'n_hidden':512,'batch_size':1}
language_model_config = {'learning_rate':0.001, 'keep_prob':0.5, 'gradient_clip':5, 'n_timesteps':1}

X = tf.placeholder(tf.int32, [rnn_model_config['batch_size'], language_model_config['n_timesteps']])
y = tf.placeholder(tf.int32, [rnn_model_config['batch_size'], language_model_config['n_timesteps']])
keep_prob = tf.placeholder(tf.float32)

if embedding_config['model_name'] == None:
	vocab = sorted(word_tokenize(training_text))
	word_to_index = {word: i for i, word in enumerate(vocab)}	
	index_to_word = dict(enumerate(vocab))
	vocab_size = len(vocab)
	input_to_rnn = tf.one_hot(X, vocab_size)
#TODO: elif: self.embedding_config.model_name == 'internal':
else:
	if 'GloVe' in embedding_config['model_name']:
		embedding = GloveEmbedding(X=X, embedding_path=embedding_config['embedding_path'])
	elif 'Word2Vec' in embedding_config['model_name']:
		embedding = Word2VecEmbedding(X=X, embedding_path=embedding_config['embedding_path'])
	embedding_look_up, vocab_size, embedding_dimensions, word_to_index, index_to_word = embedding.train()
	input_to_rnn = embedding_look_up

encoding = np.array([word_to_index[word] for word in word_tokenize(training_text)], dtype=np.int32)

if 'MultiLayeredLSTM' in rnn_model_config['model_name']:
	rnn_model = MultiLayeredLSTM(
		X=input_to_rnn, 
		n_layers=rnn_model_config['n_layers'], 
		n_hidden=rnn_model_config['n_hidden'], 
		batch_size=rnn_model_config['batch_size'], 
		keep_prob=keep_prob)
elif 'LSTM' in rnn_model_config['model_name']:
	rnn_model = LSTM(
		X=input_to_rnn, 
		n_hidden=rnn_model_config['n_hidden'],
		batch_size=rnn_model_config['batch_size'], 
		keep_prob=keep_prob)

language_model = LanguageModel(
	rnn_model=rnn_model, X=X, y=y, encoding=encoding, n_timesteps=language_model_config['n_timesteps'],
	vocab_size=vocab_size, keep_prob=keep_prob, learning_rate=language_model_config['learning_rate'], gradient_clip=language_model_config['gradient_clip'])

def pick_top_n(preds, vocab_size, top_n=5):
	p = np.squeeze(preds)
	#zero out all the probability values that are less than the top n values
	p[np.argsort(p)[:-top_n]] = 0
	#compute the average for the top n predictions (remaining are zero)
	p = p / np.sum(p)
	#draw a word from this distribution p
	word_idx = np.random.choice(vocab_size, 1, p=p)[0]
	return word_idx, p[word_idx]

def generate(given_sentence, n_next_words, rnn_model, language_model, trained_weights, word_to_index, index_to_word):
	#tf.reset_default_graph()
	#Generating the next word given the current word requires the models to accept only one word at a time
	next_words = word_tokenize(given_sentence)
	predictions = []
	saver = tf.train.Saver()
	with tf.Session() as session:
		saver.restore(session, trained_weights)
		
		new_state = session.run(rnn_model.initial_state)

		for given_word in next_words:
			x = np.zeros((1, 1))
			x[0,0] = word_to_index[given_word]
			feed = {language_model.X: x,rnn_model.keep_prob: 1.,rnn_model.initial_state: new_state}
			preds, new_state = session.run([language_model.prediction, rnn_model.final_state], feed_dict=feed)

		word_idx, pick_pred = pick_top_n(preds, language_model.vocab_size, top_n=5)
		next_words.append(index_to_word[word_idx])
		predictions.append(pick_pred)

		for i in range(n_next_words):
			x[0,0] = word_idx
			feed = {language_model.X: x,rnn_model.keep_prob: 1.,rnn_model.initial_state: new_state}
			preds, new_state = session.run([language_model.prediction, rnn_model.final_state], feed_dict=feed)

			word_idx, pick_pred = pick_top_n(preds, language_model.vocab_size)
			next_words.append(index_to_word[word_idx])
			predictions.append(pick_pred)

	return ' '.join(next_words), predictions

a = generate('agree that is',1,rnn_model, language_model, trained_weights, word_to_index, index_to_word)
print(a)
