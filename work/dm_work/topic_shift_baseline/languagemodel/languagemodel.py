#from embedding import GloveEmbedding
from .rnn import MultiLayeredLSTM
from .rnn import LSTM

import tensorflow as tf
#from nltk import word_tokenize as wt
import numpy as np
import time

class LanguageModel:
	def __init__(self, rnn_model, X, y, encoding, n_timesteps, vocab_size, keep_prob, learning_rate, gradient_clip):
		self.X = X
		self.y = y
		self.encoding = encoding
		self.n_hidden = rnn_model.n_hidden
		self.vocab_size = vocab_size
		self.n_timesteps = n_timesteps
		self.batch_size = rnn_model.batch_size

		self.keep_prob = keep_prob
		self.learning_rate = learning_rate
		self.gradient_clip = gradient_clip

		self.loss = None
		self.optimizer = None
		self.prediction = None

		self.rnn_model = rnn_model

		self.trained_weights = None
		self.trained_weights_name = None

		self.build_output()

	def build_output(self):
		hidden_state_outputs, final_state = self.rnn_model.get_output()
		self.prediction, logits = self.apply_softmax(hidden_state_outputs)
		self.loss = self.build_loss(logits)
		self.optimizer = self.build_optimizer(self.loss)

	def apply_softmax(self,hidden_state_outputs):
		# Reshape output so it's a bunch of rows, one row for each step for each sequence.
		# That is, the shape should be batch_size*num_steps rows by n_layers columns
		seq_output = tf.concat(hidden_state_outputs, axis=1)
		x = tf.reshape(seq_output, [-1, self.n_hidden])
		# Connect the RNN outputs to a softmax layer
		with tf.variable_scope('softmax'):
			softmax_w = tf.Variable(tf.truncated_normal((self.n_hidden, self.vocab_size), stddev=0.1))
			softmax_b = tf.Variable(tf.zeros(self.vocab_size))
	    
		# Since output is a bunch of rows of RNN cell outputs, logits will be a bunch
		# of rows of logit outputs, one for each step and sequence
		logits = tf.matmul(x, softmax_w) + softmax_b
	    
		# Use softmax to get the probabilities for predicted characters
		prediction = tf.nn.softmax(logits, name='predictions')
	    
	    #logits is x*W + b over the vocab, prob_dist_output is the softmax(logit), i.e., the prob distribution over vocab
		return prediction, logits


	def get_batches(self,arr,batch_size,n_timesteps):
		#Reshape data to the dimensions (batch_size X n_steps) so that it can be fed into an LSTM
		#Data should be indices
		#total number of words for each batch
		words_per_batch = batch_size * n_timesteps
		#total number of batches, each one of batch_size size
		n_batches = len(arr)//words_per_batch
		#only consider full batches, since we cannot send in the leftover in the end to an LSTM
		arr = arr[:n_batches * words_per_batch]
		#reshape the (full batches) data into a single row
		arr = arr.reshape((batch_size,-1))
		#iterate every n_timesteps and generate the corresponding (shifted by 1) labels
		for i in range(0, arr.shape[1], n_timesteps):
			#the features are the word unigrams of the sentences, every n_timesteps intervals
			X_prime = arr[:, i:i+n_timesteps]
			#the labels are the data shifted by 1; ex: input: [0 to 9] then labels: [1 to 10]
			y_prime_temp = arr[:, i+1:i+n_timesteps+1]
		    # For the very last batch, y will be one word short at the end of 
		    # the sequences which breaks things. To get around this, make an
		    # array of the appropriate size first, of all zeros, then add the targets.
		    # This will introduce a small artifact in the last batch, but it won't matter.
			y_prime = np.zeros(X_prime.shape, dtype=X_prime.dtype)
			y_prime[:,:y_prime_temp.shape[1]] = y_prime_temp

			yield X_prime, y_prime

	def build_loss(self, logits):
		# One-hot encode targets and reshape to match logits, one row per batch_size per step
		y_one_hot = tf.one_hot(self.y, self.vocab_size)
		y_reshaped = tf.reshape(y_one_hot, logits.get_shape())
    
		# Softmax cross entropy loss
		loss = tf.nn.softmax_cross_entropy_with_logits(logits=logits, labels=y_reshaped)
		loss = tf.reduce_mean(loss)
		
		return loss

	def build_optimizer(self,loss):
		# Optimizer for training, using gradient clipping to control exploding gradients
		tvars = tf.trainable_variables()
		grads, _ = tf.clip_by_global_norm(tf.gradients(loss, tvars), self.gradient_clip)
		train_op = tf.train.AdamOptimizer(self.learning_rate)
		optimizer = train_op.apply_gradients(zip(grads, tvars))

		#optimizer = tf.train.AdamOptimizer(self.learning_rate).minimize(loss)
		return optimizer

	def train(self,epochs,print_every_n,save_every_n, trained_weights_name):
		saver = tf.train.Saver(max_to_keep=100)
		with tf.Session() as session:
			session.run(tf.global_variables_initializer())

			# Use the line below to load a checkpoint and resume training
			#saver.restore(sess, 'checkpoints/______.ckpt')
			counter = 0
			for epoch in range(epochs):
				# Train network
				new_state = session.run(self.rnn_model.initial_state)
				loss = 0
				for x, y in self.get_batches(self.encoding, self.batch_size, self.n_timesteps):
					counter += 1
					start = time.time()
					feed = {self.X: x,
							self.y: y,
							self.rnn_model.keep_prob: self.keep_prob,
							self.rnn_model.initial_state: new_state}
					batch_loss, new_state, _ = session.run([self.loss, self.rnn_model.final_state, self.optimizer], feed_dict=feed)
					if (counter % print_every_n == 0):
						end = time.time()
						print('Epoch: {}/{}... '.format(epoch+1, epochs),
							'Training Step: {}... '.format(counter),
							'Training loss: {:.4f}... '.format(batch_loss),
							'{:.4f} sec/batch'.format((end-start)))
		        
					if (counter % save_every_n == 0):
						saver.save(session, trained_weights_name + "_checkpoints/i{}_l{}.ckpt".format(counter, self.n_hidden))

			saver.save(session, trained_weights_name + "_checkpoints/i{}_l{}.ckpt".format(counter, self.n_hidden))
		self.trained_weights = tf.train.latest_checkpoint(trained_weights_name + "_checkpoints")
		self.trained_weights_name = trained_weights_name

	def get_trained_weights(self):
		return self.trained_weights

	def get_trained_weights_name(self):
		return self.trained_weights_name

'''from nltk import word_tokenize as wt
training_file = 'test.txt'
with open(training_file,'r') as f:
	text = f.read()
vocab = sorted(wt(text))
vocab_to_index = {c: i for i, c in enumerate(vocab)}
index_to_vocab = dict(enumerate(vocab))

tf.reset_default_graph()

n_timesteps = 3
batch_size = 20
X = tf.placeholder(tf.int32, [batch_size, n_timesteps], name='inputs')
y = tf.placeholder(tf.int32, [batch_size, n_timesteps], name='labels')
keep_prob = tf.placeholder(tf.float32, name='keep_prob')

gloveEmbedding = GloveEmbedding(X=X, embedding_path='glove.6B/glove.6B.50d.txt')
embedding_look_up, vocab_size, embedding_dimensions, word_to_index, index_to_word = gloveEmbedding.train()
encoding = np.array([word_to_index[word] for word in wt(text)], dtype=np.int32)
vocab_size = len(word_to_index.keys())

x_one_hot = tf.one_hot(X, vocab_size)

n_layers = 2
n_hidden = 512
multi_layered_lstm = MultiLayeredLSTM(
	X=embedding_look_up, n_layers=n_layers, n_hidden=n_hidden, batch_size=batch_size, keep_prob=keep_prob)

learning_rate = 0.001
keep_prob = 0.5
gradient_clip = 5
lm = LanguageModel(
	rnn_model=multi_layered_lstm, X=X, y=y, encoding=encoding, n_timesteps=n_timesteps,
	vocab_size=vocab_size, keep_prob=keep_prob, learning_rate=learning_rate, gradient_clip=gradient_clip)
lm.train(epochs=20,print_every_n=1,save_every_n=10,trained_weights_name='test')'''