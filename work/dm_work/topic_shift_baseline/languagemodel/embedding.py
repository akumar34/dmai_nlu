import tensorflow as tf
import abc
from abc import ABCMeta, abstractmethod

import gensim
from gensim.test.utils import datapath, get_tmpfile
from gensim.models import KeyedVectors
from gensim.scripts.glove2word2vec import glove2word2vec

import os

from collections import defaultdict

import numpy as np

class Embedding(metaclass=ABCMeta):
    __metaclass__ = abc.ABCMeta

    def __init__(self, X, embedding_path, index_to_word=None, word_to_index=None):
        self.embedding_path = embedding_path
        
        self.word_to_index = word_to_index
        self.index_to_word = index_to_word
        
        self.word_to_embedding = None
        self.index_to_embedding = None

        self.index_to_embedding_lookup = None

        self.X = X

        self.build_output()

    def get_word_to_index(self):
        return self.word_to_index

    def get_index_to_word(self):
        return self.index_to_word

    def get_index_to_embedding(self):
        return self.index_to_embedding

    def get_word_to_embedding(self):
        return self.word_to_embedding

    def get_vocab_size(self):
        return self.index_to_embedding.shape[0]

    def get_dim(self):
        return self.index_to_embedding.shape[1]

    def get_index_to_embedding_lookup(self):
        return self.index_to_embedding_lookup

    @abc.abstractmethod
    def build_output(self):
        pass

    def get_output(self):
        #return self.tf_embedding_assign, self.embedding_word_ids, get_vocab_size(), get_dim()
        return self.get_index_to_embedding_lookup(), self.get_vocab_size(), self.get_dim(), self.get_word_to_index(), self.get_index_to_word()

    def train(self):
        #Variable that will hold the embedding is tf_embedding (that is, the weight parameter)
        tf_embedding = tf.Variable(tf.constant(0.0, shape=self.index_to_embedding.shape),trainable=False,name="Embedding")
        #now setup a placeholder for the index 2 embedding dictionary, but only when the session is run
        tf_embedding_placeholder = tf.placeholder(tf.float32, shape=self.index_to_embedding.shape)
        #finally tf_embedding weight parameter will be assigned to the index 2 embedding dictionary when session is run
        #the assignment to the weight parameter will be executed on tf_embedding_init reference.
        tf_embedding_assign = tf_embedding.assign(tf_embedding_placeholder)

        with tf.Session() as session:
            session.run(tf.global_variables_initializer())
            #run session on tf_embedding_unit so that it can be assigned the index 2 embedding. 
            #now we have the full tf_embedding in tensorflow loaded with GloVe!
            _ = session.run(tf_embedding_assign, feed_dict={tf_embedding_placeholder: self.index_to_embedding})

        #done loading glove weights, so now provide index_to_embedding_lookup table that can access the weights (given word index)        
        self.index_to_embedding_lookup = tf.nn.embedding_lookup(tf_embedding, self.X)

        return self.get_output()

class GloveEmbedding(Embedding):
    def __init__(self,X, embedding_path, index_to_word=None, word_to_index=None):
        super().__init__(X, embedding_path, index_to_word, word_to_index)

    def build_output(self):
        #get file name
        embedding_file_name = None
        if 'word2vec' not in os.path.basename(self.embedding_path):
            #gensim only uses word2vec format, so create temporary word2vec file that will later on populate with correct format of glove
            tmp_file = get_tmpfile('word2vec.' + ''.join(os.path.basename(self.embedding_path).split('.')[1:]))
            #transform glove file to word2vec and store in tmp_file
            glove2word2vec(self.embedding_path, tmp_file)
            embedding_file_name = tmp_file
        else:
            embedding_file_name = self.embedding_path
        #load the glove embedding from word2vec tmp_file
        embedding = KeyedVectors.load_word2vec_format(embedding_file_name)
        #for any words not found, just assign all zeros.
        _WORD_NOT_FOUND = [0.0]* len(embedding['the'])

        #since word_to_index and index_to_word are provided, let us use that to build the embedding weights
        if self.word_to_index is None and self.index_to_word is None:
            self.index_to_word = embedding.wv.index2word            
            self.word_to_index = {word:index for index,word in enumerate(self.index_to_word)}

        #create the word index to word embedding array
        self.index_to_embedding = [embedding.wv[word] for _,word in enumerate(self.index_to_word)]
        #the final element in this array is all zeros to represent unknown words
        self.index_to_embedding = np.array(self.index_to_embedding + [_WORD_NOT_FOUND])

        _LAST_INDEX = len(self.index_to_embedding)
        #add one more index to the dictionary as a placeholder, (the embedding vocab size + 1)
        self.word_to_index = defaultdict(lambda: _LAST_INDEX, self.word_to_index)

        #create word to embedding dictionary
        self.word_to_embedding = {word:embedding.wv[word] for index,word in enumerate(self.index_to_word)}
        #add one more element in dictionary corresponding to unkown word
        self.word_to_embedding = defaultdict(lambda: _WORD_NOT_FOUND)

        #index to word array
        self.index_to_word = [word for _,word in enumerate(self.index_to_word)] + ['unk']

        del embedding

class Word2VecEmbedding(Embedding):
    def __init__(self,X, embedding_path, index_to_word=None, word_to_index=None):
        super().__init__(X, embedding_path, index_to_word, word_to_index)

    def build_output(self):
       #get file name
        embedding_file_name = self.embedding_path
        #load the glove embedding from word2vec tmp_file
        embedding = KeyedVectors.load_word2vec_format(embedding_file_name)
        #for any words not found, just assign all zeros.
        _WORD_NOT_FOUND = [0.0]* len(embedding['the'])

        #since word_to_index and index_to_word are provided, let us use that to build the embedding weights
        if self.word_to_index is None and self.index_to_word is None:
            self.index_to_word = embedding.wv.index2word            
            self.word_to_index = {word:index for index,word in enumerate(index_to_word)}

        #create the word index to word embedding array
        self.index_to_embedding = [embedding.wv[word] for _,word in enumerate(self.index_to_word)]
        #the final element in this array is all zeros to represent unknown words
        self.index_to_embedding = np.array(self.index_to_embedding + [_WORD_NOT_FOUND])

        _LAST_INDEX = len(self.index_to_embedding)
        #add one more index to the dictionary as a placeholder, (the embedding vocab size + 1)
        self.word_to_index = defaultdict(lambda: _LAST_INDEX, self.word_to_index)

        #create word to embedding dictionary
        self.word_to_embedding = {word:embedding.wv[word] for index,word in enumerate(self.index_to_word)}
        #add one more element in dictionary corresponding to unkown word
        self.word_to_embedding = defaultdict(lambda: _WORD_NOT_FOUND)

        #index to word array
        self.index_to_word = [word for _,word in enumerate(self.index_to_word)] + ['unk']

        del embedding


#Load GloVe embedding weights to the appropriate index dictionaries and arrays
'''from nltk import word_tokenize
from collections import Counter

tf.reset_default_graph()

training_file = 'test.txt'
with open(training_file,'r') as f:
    training_text = f.read()

word_freq = Counter(word_tokenize(training_text))
vocab_size = len(word_freq)
vocab = word_freq.most_common(vocab_size-1)
# = len(vocab)
index_to_word = [x[0] for x in vocab]
word_to_index = dict([(w,i) for i,w in enumerate(index_to_word)])

#index_to_word=None
#word_to_index=None

X = tf.placeholder(tf.int32, [None, None], name='inputs')
gloveEmbedding = GloveEmbedding(X, embedding_path='glove.6B/word2vec.6B.50d.txt', index_to_word=index_to_word, word_to_index=word_to_index)
index_to_embedding_lookup, vocab_size, dimensions, word_to_index, index_to_word = gloveEmbedding.train()

print(vocab_size, dimensions)'''