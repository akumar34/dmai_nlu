import abc
from abc import ABCMeta, abstractmethod

import tensorflow as tf

class RNN(metaclass=ABCMeta):
	__metaclass__ = abc.ABCMeta
	#if you do not wish to use dropout, simply set keep_prob=None
	def __init__(self, X, n_hidden, keep_prob, batch_size):
		self.X = X
		self.n_hidden = n_hidden
		self.batch_size = batch_size
		self.keep_prob = keep_prob

		self.hidden_state_outputs = None
		self.final_state = None
		self.initial_state = None

		self.build_output()

	@abc.abstractmethod
	def build_output(self):
		pass

	def get_cell(self):
		#let us create the lstm next
		lstm = tf.nn.rnn_cell.BasicLSTMCell(self.n_hidden)

		if self.keep_prob == None:
			return lstm

		#and dropout regularization
		drop = tf.nn.rnn_cell.DropoutWrapper(lstm, output_keep_prob=self.keep_prob)

		return drop

	def get_output(self):
		return self.hidden_state_outputs, self.final_state

	def get_name(self):
		return self.name

class LSTM(RNN):
	def __init__(self, X, n_hidden, keep_prob, batch_size):
		self.name = 'lstm'
		super().__init__(X, n_hidden, keep_prob, batch_size)

	def build_output(self):
		#make it multi-layered LSTM
		cell = self.get_cell()
		#getting an initial state of all zeros (dimensions n_hidden X 1). There will be batch_size of these.
		self.initial_state = cell.zero_state(self.batch_size, tf.float32)
		#the forward pass to the RNN has outputs (cell memory) and final_state
		self.hidden_state_outputs, self.final_state = tf.nn.dynamic_rnn(cell, self.X, initial_state=self.initial_state)
		#outputs contains the state output for each of the n_timesteps. It has this for each batch.
		#outputs[-1] contains the state output of the final state.

class MultiLayeredLSTM(RNN):
	def __init__(self, X, n_layers, n_hidden, keep_prob, batch_size):
		self.name = 'multilayeredlstm'
		self.n_layers = n_layers
		super().__init__(X, n_hidden, keep_prob, batch_size)
    	
	def build_output(self):
		#make it multi-layered LSTM
		cell = tf.nn.rnn_cell.MultiRNNCell([self.get_cell() for _ in range(self.n_layers)])
		#getting an initial state of all zeros. Remember that 
		self.initial_state = cell.zero_state(self.batch_size, tf.float32)
		#the forward pass to the RNN
		self.hidden_state_outputs, self.final_state = tf.nn.dynamic_rnn(cell, self.X, initial_state=self.initial_state)

class BiDirectionalLSTM(RNN):
	def __init__(self, X, n_layers, n_hidden, keep_prob, batch_size):
		self.name = 'multilayeredlstm'
		self.n_layers = n_layers
		super().__init__(X, n_hidden, keep_prob, batch_size)
    	
	def build_output(self):
		#make it multi-layered LSTM
		cell = tf.nn.rnn_cell.MultiRNNCell([self.get_cell() for _ in range(self.n_layers)])
		#getting an initial state of all zeros. Remember that 
		self.initial_state = cell.zero_state(self.batch_size, tf.float32)
		#the forward pass to the RNN
		self.hidden_state_outputs, self.final_state = tf.nn.dynamic_rnn(cell, self.X, initial_state=self.initial_state)


#Test 1: multilayered lstm requires input to have dimensions n_timesteps X batch_size X vocab_size
#n_timesteps = 3
#batch_size = 50
#vocab_size = 50000

#X = tf.placeholder(tf.int32, [batch_size, n_timesteps], name='inputs')
#x_one_hot = tf.one_hot(X, vocab_size)
#print(x_one_hot.shape)
#multi_layered_lstm = MultiLayeredLSTM(X = x_one_hot, n_layers = 2, n_hidden=512, batch_size=batch_size, keep_prob=0.7)
#hidden_state_outputs, final_state = multi_layered_lstm.get_output()

#Test 2: multilayered lstm with glove embedding
#tf.reset_default_graph()

#X = tf.placeholder(tf.int32, [None, None], name='inputs')
#gloveEmbedding = GloveEmbedding(X, embedding_path='glove.6B/glove.6B.50d.txt')
#embedding_look_up, vocab_size, embedding_dimensions = gloveEmbedding.train()
#print(vocab_size)
#print(embedding_dimensions)
#print(embedding_look_up.shape)
#multi_layered_lstm = MultiLayeredLSTM(X = embedding_look_up, n_layers = 2, n_hidden=512, batch_size=50, keep_prob=0.7)
#hidden_state_outputs, final_state = multi_layered_lstm.get_output()