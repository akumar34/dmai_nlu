from swda import *

corpus = CorpusReader('swda/swda')

topic_counts = dict()
for trans in corpus.iter_transcripts(display_progress=True):
	if trans.topic_description not in topic_counts:
		topic_counts[trans.topic_description] = []

	#print((trans.prompt, trans.topic_description))
	
	for utt in trans.utterances:
		topic_counts[trans.topic_description].append(utt.text)

	#print('\n')

for (key,val) in topic_counts.items():
	print(key,val)
	print('\n')

