import sys, nltk

for line in sys.stdin:
    for sentence in nltk.sent_tokenize(line):
        print(' '.join([word.lower().strip() for word in nltk.word_tokenize(sentence)]).lower())
