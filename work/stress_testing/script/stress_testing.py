import locust.stats
locust.stats.CSV_STATS_INTERVAL_SEC = 10

from locust import HttpLocust, TaskSet, task
from requests.exceptions import ConnectionError

class MyTaskSet(TaskSet):
	#List each task that lotust will randomly choose from
    @task
    def my_task(self):
        print("Locust instance (%r) executing my_task" % (self.locust))
        try:
            response = self.client.get(url="/parse?q=can you tell me some cheap indian restuarants up north")
        except ConnectionError as e:
            print("Connection refused by the server..",e)
            print("Let me sleep for 10 seconds")
            print("ZZzzzz...")
            time.sleep(10)
            print("Was a nice sleep, now let me continue...")

        #print(response.content)

#The brain decides when to call a random task from the set of all tasks
class MyLocust(HttpLocust):
    task_set = MyTaskSet
    min_wait = 1000 #Wait between 1 and 1 seconds per random call of a task from the task set
    max_wait = 1000
    #host="http://localhost:5000"
    host="http://ec2-18-217-226-183.us-east-2.compute.amazonaws.com:5000"
