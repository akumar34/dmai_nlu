import subprocess
from subprocess import Popen
import shlex
import time
import numpy as np
import psutil
import os

execution_tests = [
	(5000,1000,30000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
]
execution_tests = [
	#1 thread, spacy model
	(10,1,1000,1, 'models/dstc2/spacy/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
	(500,100,10000,1, 'models/dstc2/spacy/'),
	(1000,400,20000,1, 'models/dstc2/spacy/'),
	(1500,500,20000,1, 'models/dstc2/spacy/'),
	(2000,500,20000,1, 'models/dstc2/spacy/'),
	(5000,1000,30000,1, 'models/dstc2/spacy/'),
	#2 threads, spacy model
	(10,1,1000,2, 'models/dstc2/spacy/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
	(500,100,10000,2, 'models/dstc2/spacy/'),
	(1000,400,20000,2, 'models/dstc2/spacy/'),
	(1500,500,20000,2, 'models/dstc2/spacy/'),
	(2000,500,20000,2, 'models/dstc2/spacy/'),
	(5000,1000,30000,2, 'models/dstc2/spacy/'),
	#4 threads, spacy model	
	(10,1,1000,4, 'models/dstc2/spacy/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
	(500,100,10000,4, 'models/dstc2/spacy/'),
	(1000,400,20000,4, 'models/dstc2/spacy/'),
	(1500,500,20000,4, 'models/dstc2/spacy/'),
	(2000,500,20000,4, 'models/dstc2/spacy/'),
	(5000,1000,30000,4, 'models/dstc2/spacy/'),
	#1 thread, tfe model
	(10,1,1000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
	(500,100,10000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1000,400,20000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1500,500,20000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(2000,500,20000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(5000,1000,30000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	#2 threads, tfe model
	(10,1,1000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
	(500,100,10000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1000,400,20000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1500,500,20000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(2000,500,20000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(5000,1000,30000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	#4 threads, tfe model	
	(10,1,1000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
	(500,100,10000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1000,400,20000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1500,500,20000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(2000,500,20000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(5000,1000,30000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
]

'''execution_tests = [
	#1 thread, spacy model
	(1,1,100,1, 'models/dstc2/spacy/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
	(50,25,1000,1, 'models/dstc2/spacy/'), 
	(200,50,10000,1, 'models/dstc2/spacy/'),
	(400,100,10000,1, 'models/dstc2/spacy/'),
	(500,100,10000,1, 'models/dstc2/spacy/'),
	(800,100,10000,1, 'models/dstc2/spacy/'),
	(1000,200,20000,1, 'models/dstc2/spacy/'),
	(1200,400,20000,1, 'models/dstc2/spacy/'),
	(1400,400,20000,1, 'models/dstc2/spacy/'),
	(1600,400,20000,1, 'models/dstc2/spacy/'),
	(1800,400,20000,1, 'models/dstc2/spacy/'),
	(2000,500,20000,1, 'models/dstc2/spacy/'),
	#2 threads, spacy model
	(1,1,100,2, 'models/dstc2/spacy/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
	(50,25,1000,2, 'models/dstc2/spacy/'), 
	(200,50,10000,2, 'models/dstc2/spacy/'),
	(400,100,10000,2, 'models/dstc2/spacy/'),
	(500,100,10000,2, 'models/dstc2/spacy/'),
	(800,100,10000,2, 'models/dstc2/spacy/'),
	(1000,200,20000,2, 'models/dstc2/spacy/'),
	(1200,400,20000,2, 'models/dstc2/spacy/'),
	(1400,400,20000,2, 'models/dstc2/spacy/'),
	(1600,400,20000,2, 'models/dstc2/spacy/'),
	(1800,400,20000,2, 'models/dstc2/spacy/'),
	(2000,500,20000,2, 'models/dstc2/spacy/'),
	#4 threads, spacy model	
	(1,1,100,4, 'models/dstc2/spacy/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
	(50,25,1000,4, 'models/dstc2/spacy/'), 
	(200,50,10000,4, 'models/dstc2/spacy/'),
	(400,100,10000,4, 'models/dstc2/spacy/'),
	(500,100,10000,4, 'models/dstc2/spacy/'),
	(800,100,10000,4, 'models/dstc2/spacy/'),
	(1000,200,20000,4, 'models/dstc2/spacy/'),
	(1200,400,20000,4, 'models/dstc2/spacy/'),
	(1400,400,20000,4, 'models/dstc2/spacy/'),
	(1600,400,20000,4, 'models/dstc2/spacy/'),
	(1800,400,20000,4, 'models/dstc2/spacy/'),
	(2000,500,20000,4, 'models/dstc2/spacy/'),
	#8 threads, spacy model
	(1,1,100,8, 'models/dstc2/spacy/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
	(50,25,1000,8, 'models/dstc2/spacy/'), 
	(200,50,10000,8, 'models/dstc2/spacy/'),
	(400,100,10000,8, 'models/dstc2/spacy/'),
	(500,100,10000,8, 'models/dstc2/spacy/'),
	(800,100,10000,8, 'models/dstc2/spacy/'),
	(1000,200,20000,8, 'models/dstc2/spacy/'),
	(1200,400,20000,8, 'models/dstc2/spacy/'),
	(1400,400,20000,8, 'models/dstc2/spacy/'),
	(1600,400,20000,8, 'models/dstc2/spacy/'),
	(1800,400,20000,8, 'models/dstc2/spacy/'),
	(2000,500,20000,8, 'models/dstc2/spacy/'),
	#1 thread, tfe model
	(1,1,100,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
	(50,25,1000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'), 
	(200,50,10000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(400,100,10000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(500,100,10000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(800,100,10000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1000,200,20000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1200,400,20000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1400,400,20000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1600,400,20000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1800,400,20000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(2000,500,20000,1, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	#2 threads, tfe model
	(1,1,100,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
	(50,25,1000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'), 
	(200,50,10000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(400,100,10000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(500,100,10000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(800,100,10000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1000,200,20000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1200,400,20000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1400,400,20000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1600,400,20000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1800,400,20000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(2000,500,20000,2, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	#4 threads, tfe model	
	(1,1,100,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
	(50,25,1000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'), 
	(200,50,10000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(400,100,10000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(500,100,10000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(800,100,10000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1000,200,20000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1200,400,20000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1400,400,20000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1600,400,20000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1800,400,20000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(2000,500,20000,4, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	#8 threads, tfe model	
	(1,1,100,8, 'models/dstc2/tensorflow_embedding_multiple_intents/'), #locust number of users, locust hatch rate, locust max requests, rasa nlu threads, rasa nlu model
	(50,25,1000,8, 'models/dstc2/tensorflow_embedding_multiple_intents/'), 
	(200,50,10000,8, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(400,100,10000,8, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(500,100,10000,8, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(800,100,10000,8, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1000,200,20000,8, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1200,400,20000,8, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1400,400,20000,8, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1600,400,20000,8, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(1800,400,20000,8, 'models/dstc2/tensorflow_embedding_multiple_intents/'),
	(2000,500,20000,8, 'models/dstc2/tensorflow_embedding_multiple_intents/'),

]'''

def kill_process(process_name):
	cmd='ps aux | grep "' + process_name + '"'
	tasks = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE)
	tasks.wait()
	tasks = tasks.communicate()
	tasks = tasks[0].decode('utf-8').split('\n')
	pid = -1
	found_task = False
	for task in tasks:
		if 'python' in task:
			print("Found " + str(task))
			pid = task.split()[1]
			print(process_name + " is currently running [pid=" + str(pid) + "] so killing it")

			cmd = 'kill -9 ' + str(pid)
			subprocess.call(cmd,shell=True)
			found_task = True
	if found_task == False:
		print(process_name + " is not currently running")

def run_cmd(cmd,log_file):
	log_f = open(log_file,'w')
	formatted_cmd = shlex.split(cmd)
	
	p = subprocess.Popen(formatted_cmd, stdout=log_f, stderr=log_f)

	return p

def run_rasa_nlu(log_file, model, num_threads = 1):
	cmd = 'python -m rasa_nlu.server --path ' + str(model) + ' --num_threads ' + str(num_threads)

	print("Starting RASA_NLU server with command [" + cmd + "]")

	return run_cmd(cmd,log_file)

def run_locust(log_file, stress_test_file,num_users=500,hatch_rate=50,num_max_requests=10000):
	cmd = 'locust -f ' + str(stress_test_file) + ' --no-web -c ' + str(num_users) + ' -r ' + str(hatch_rate) + ' --print-stats --logfile=./my.log.locust --num-request ' + str(num_max_requests)
	
	print("Starting Locust with command [" + cmd + "]")
	
	return run_cmd(cmd,log_file)

for test in execution_tests:
	#kill_process("locust")

	#Set Rasa NLU parameters even if not calling. This will be useful for downstream compilation of results.
	model=test[4]
	num_threads = test[3]

	#Run Locust in its own process
	stress_test_file='./stress_testing.py'
	num_users=test[0]
	hatch_rate=test[1]
	num_max_requests=test[2]
	log_file = 'stat_logs/my.log.' + str(num_users) + '_' + str(hatch_rate) + '_' + str(num_threads)
	print("Generating log file " + log_file)

	locust_process = run_locust(log_file, stress_test_file, num_users, hatch_rate, num_max_requests)
	locust_process_pid = locust_process.pid
	print("Locust server started with process id [" + str(locust_process_pid) + "]")

	#While Locust process is running, just wait since we cannot move forward with the stat computations
	locust_process.wait()

	#Gracefully close the Rasa NLU and locust processes since stress test is over
	locust_process.terminate()

	#Process the stats from Locust run so we can write the results to file	
	stats=[]
	with open(log_file,'rt') as f:
		print('method\tname\t#reqs\t#fails\tavg rt\tmin rt\tmax rt\tmed rt\trps\tmem\tcpu')
		for line in f:
			if '-' in line:
				continue
			if line.strip() == '':
				continue
			if 'Name' in line:
				continue
			if 'Total' in line:
				continue
			if 'Percentage' in line:
				continue
			if 'Error report' in line:
				break

			data = line.strip().split()
			data.append(0.0)
			data.append(0.0)
			
			if len(data) > 12:
				continue

			#method, name, total_requests, total_fails, average_response_time, min_response_time, max_response_time, _ , median_response_time, requests_per_second, ram_memory, cpu_percent = data
			data[3] = data[3].split('(')[0]
			data.remove('|')

			#data = (method, name, total_requests, total_fails, average_response_time, min_response_time, max_response_time, median_response_time, requests_per_second, ram_memory, cpu_percent)
			stats.append(data)

			print(data)

	#Calculate the stat averages for the stress test run
	use_prev_hist = min(len(stats),5)
	print("Using the " + str(use_prev_hist) + " previous values for stat average calculations\n")

	avg_fails = np.mean([float(val[3]) for val in stats])
	avg_response_time = np.mean([float(val[4]) for val in stats[len(stats)-use_prev_hist:]])
	avg_rps = np.mean([float(val[8]) for val in stats[len(stats)-use_prev_hist:]])
	avg_ram_memory = 0.0
	avg_cpu_percent = 0.0

	graphical_data = (num_users, hatch_rate, num_threads, model, avg_fails, avg_response_time, avg_rps, avg_ram_memory, avg_cpu_percent)

	#Write stat averages to the results.csv file
	if os.path.isfile('locust_results.csv'):
		results = ""
	else:
		results = "NUM_USERS;HATCH_RATE;NUM_THREADS;NLU_MODEL;AVG_FAILS;AVG_RESP_TIME;AVG_RPS;AVG_MEM;AVG_CPU\n"

	print("NUM USERS [" + str(graphical_data[0]) + "] HATCH RATE [" + str(graphical_data[1]) + "] NUM_THREADS [" + str(graphical_data[2]) + "] NLU MODEL [" + graphical_data[3] + "] AVG FAILS [" + str(graphical_data[4]) + "] AVG RESPONSE TIME [" + str(graphical_data[5]) + "] AVG RPS [" + str(graphical_data[6]) + "] AVG RAM MEMORY [" + str(graphical_data[7]) + "] AVG CPU PERCENT [" + str(graphical_data[8]) + "]")
	results += str(graphical_data[0]) + ";" + str(graphical_data[1]) + ";" + str(graphical_data[2]) + ";" + graphical_data[3] + ";" + str(graphical_data[4]) + ";" + str(graphical_data[5]) + ";" + str(graphical_data[6]) + ";" + str(graphical_data[7]) + ";" + str(graphical_data[8]) + "\n"

	with open('locust_results.csv','a') as f:
		f.write(results)
